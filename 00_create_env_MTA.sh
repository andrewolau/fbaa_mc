#!/bin/bash
conda create -n MTA ipykernel -y
source activate MTA

python -m pip install git+https://andrewolau@bitbucket.org/wx_rds/mta.git
pip install --upgrade 'google-cloud-bigquery[bqstorage,pandas]'
pip install fsspec gcsfs
conda install seaborn -y
