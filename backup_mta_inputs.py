import smtplib 
from email.message import EmailMessage
import pandas as pd
# block to setup BQ
import os
import google.auth
from google.cloud import bigquery
from google.cloud import bigquery_storage
import datetime

os.environ['GOOGLE_APPLICATION_CREDENTIALS']=f"/home/jovyan/.config/gcloud/legacy_credentials/{os.getenv('JUPYTERHUB_USER')}/adc.json" 

# Explicitly create a credentials object. This allows you to use the same
# credentials for both the BigQuery and BigQuery Storage clients, avoiding
# unnecessary API calls to fetch duplicate authentication tokens.
credentials, your_project_id = google.auth.default(
    scopes=["https://www.googleapis.com/auth/cloud-platform"]
)

# Make clients.
bqclient = bigquery.Client(credentials=credentials, project='wx-bq-poc',)
bqstorageclient = bigquery_storage.BigQueryReadClient(credentials=credentials)
today = datetime.date.today()
last_monday = today + datetime.timedelta(days=-today.weekday(), weeks=-1)

def backup_MTA_source_tables(fw):
    """
    Overview
        Backs up input source tables to the MTA process. These are tables that are overwritten every week.
        Run every week after the usual MTA process. Will not overwrite last week's tables.
    Arguments
        fw - financial week   
    Returns
        summary of backed up tables
    """
    query_string = """
    -- backing up tables
    create table `wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_dacamp_prod_event_mw_{fw}` as
    select * from `wx-bq-poc.digital_attribution_modelling.dacamp_prod_event_mw`;

    create table `wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_dacamp_prod_activation_{fw}` as
    select * from `wx-bq-poc.digital_attribution_modelling.dacamp_prod_activation`;

    create table `wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_dacamp_prod_online_sales_{fw}` as
    select * from `wx-bq-poc.digital_attribution_modelling.dacamp_prod_online_sales`;

    create table `wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_dacamp_prod_mc_final_{fw}` as
    select * from `wx-bq-poc.digital_attribution_modelling.dacamp_prod_mc_final`;

    create table `wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_dacamp_prod_mc_final_crn_{fw}` as
    select * from `wx-bq-poc.digital_attribution_modelling.dacamp_prod_mc_final_crn`;
    
    create table `wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_marketable_crn_{fw}` as
    select * from `wx-bq-poc.digital_attribution_modelling.marketable_crn`;

    -- summary to check if tables were made
    select "dacamp_prod_event_mw" as table, count(*) as count from `wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_dacamp_prod_event_mw_{fw}`
    UNION ALL
    select "dacamp_prod_activation" as table, count(*) as count from `wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_dacamp_prod_activation_{fw}`
    UNION ALL
    select "dacamp_prod_online_sales" as table, count(*) as count from `wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_dacamp_prod_online_sales_{fw}`
    UNION ALL
    select "dacamp_prod_mc_final" as table, count(*) as count from `wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_dacamp_prod_mc_final_{fw}`
    UNION ALL
    select "dacamp_prod_mc_final_crn" as table, count(*) as count from `wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_dacamp_prod_mc_final_crn_{fw}`
    UNION ALL
    select "marketable_crn" as table, count(*) as count from `wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_marketable_crn_{fw}`
    ;    
    """.format(fw=fw)
    print("""
    BQ row count check should look something like:
    0	dacamp_prod_mc_final	400
    1	dacamp_prod_online_sales	300,000
    2	dacamp_prod_mc_final_crn	6m
    3	dacamp_prod_activation	5m
    4	dacamp_prod_event_mw	28m
    5	marketable_crn	10m
    """)
    results = (
        bqclient.query(query_string)
        .result()
        .to_dataframe(bqstorage_client=bqstorageclient)
    )
    return results

print("Running for ", str(last_monday))
result = backup_MTA_source_tables(str(last_monday))


print("Sending out results email")
EmailAdd = "woolworths.bot001@gmail.com" #senders Gmail id over here
Pass = "%JKg5rtwoPZ*v&fUxe8U" # this is a thowaway e-mail account

msg = EmailMessage()
msg['Subject'] = 'woolworths.bot: backup of MTA tables for fw ' +  str(last_monday) # Subject of Email
msg['From'] = EmailAdd
msg['To'] = 'alau3@woolworths.com.au' # Reciver of the Mail
msg.set_content("""
Tables created:

`wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_dacamp_prod_event_mw_{fw}`
`wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_dacamp_prod_activation_{fw}`
`wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_dacamp_prod_online_sales_{fw}`
`wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_dacamp_prod_mc_final_{fw}`
`wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_dacamp_prod_mc_final_crn_{fw}`
`wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_marketable_crn_{fw}`

Row counts of tables backed up:
{results_table}

BQ row count check should look something like:
0	dacamp_prod_mc_final	400
1	dacamp_prod_online_sales	300,000
2	dacamp_prod_mc_final_crn	6m
3	dacamp_prod_activation	5m
4	dacamp_prod_event_mw	28m
5	marketable_crn	10m
""".format(results_table=str(result)), fw=fw) # Email body or Content

#### >> Code from here will send the message << ####
with smtplib.SMTP_SSL('smtp.gmail.com', 465) as smtp: #Added Gmails SMTP Server
    smtp.login(EmailAdd,Pass) #This command Login SMTP Library using your GMAIL
    smtp.send_message(msg) #This Sends the message