#!/bin/bash
# install from local
# 1. sync on bit bucket
# 2. git pull main
# 3. run this

# conda create -n MTA_AL ipykernel -y
source activate MTA_AL

pip uninstall -y mta
# clean up old SQL files
rm -r ~/my-conda-envs/MTA_AL/lib/python3.9/site-packages/fbaa/sql

yes | python -m pip install ~/02_nash/module_mta_AL/mta

# pip install --upgrade 'google-cloud-bigquery[bqstorage,pandas]'
# pip install fsspec gcsfs
# pip install pandas-gbq
# conda install seaborn -y
