from fbaa.pipeline import CampaignAttributionPipeline
import pandas as pd
# import sys

# sys.stdout = open('log.txt', 'w')

def test_campaign_logic_pipeline():
    """ Execution test for the Campaign Logic Pipeline """
    campaign_code = "CVM-3510"
    campaign_start = "2020-01-07"
    campaign_end = "2021-01-10"
#     fb_campaign_id = "6221076463031,6221072558831"
#     fb_touch_days = 3
    attribution = CampaignAttributionPipeline(
        campaign_code,
        campaign_start,
        campaign_end
#         fb_campaign_id,
#         fb_touch_days,
#         campaign_start,
    )
    print(attribution)


res = test_campaign_logic_pipeline()

out_df = pd.DataFrame(res)
out_df.to_csv("res_" + campaign_code + "_" + campaign_start + ".csv")

# sys.stdout.close()
